package br.ufc.dc.dspm.mynotes;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Created by Vasco on 14/06/2016.
 */
public class EditarNote extends AppCompatActivity {

    private NoteDAO noteDAO;
    Note note;
    static int dia;
    static int mes;
    static int ano;

    EditText edt1;
    EditText edt2;

    Button date;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editar_note);
        getSupportActionBar();

        noteDAO = new NoteDAO(this);


        Integer id = Integer.parseInt(getIntent().getStringExtra("id"));

        try {
            this.note = noteDAO.retrieve(id);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        edt1 = (EditText) findViewById(R.id.editText);
        edt2 = (EditText) findViewById(R.id.editText2);
        date = (Button) findViewById(R.id.button2);

        edt1.setText(note.getTitle());
        edt2.setText(note.getContent());
        date.setText(note.getDateString());


    }

/*    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String id = data.getStringExtra("data");
        System.out.println("$$$$$$$$$$$:"+id);
        try {
            Note noteRetrieve = noteDAO.retrieve(Integer.parseInt(id));
            this.note = noteRetrieve;
            this.recreate();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editar_note, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.salvar){
            //Salvar no banco, atualizar nó
            Calendar calendar = Calendar.getInstance();
            calendar.set(ano, mes, dia);

            note.setTitle(edt1.getText().toString());
            note.setContent(edt2.getText().toString());
            note.setDate(calendar);

            noteDAO.update(this.note);
            Toast.makeText(this,"Atualizado com sucesso",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,MyNotes.class);
            startActivity(intent);
            finish();
        }
        else if(item.getItemId() == R.id.excluir){
            //Excluir do banco
            noteDAO.delete(note.getId());
            Toast.makeText(this,"Excluido com sucesso",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,MyNotes.class);
            startActivity(intent);
            finish();

        }

        return super.onOptionsItemSelected(item);
    }

    public void showDateEditorDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            dia = day;
            mes = month;
            ano = year;
        }
    }
}
