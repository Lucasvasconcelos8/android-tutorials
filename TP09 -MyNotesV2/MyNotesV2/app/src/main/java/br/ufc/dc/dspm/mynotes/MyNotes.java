package br.ufc.dc.dspm.mynotes;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class MyNotes extends FragmentActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    ListView listView;
    int dia;
    int mes;
    int ano;

    List<String> list = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_notes);

        listView = (ListView)findViewById(R.id.listViewNotes);

        atualizarListView();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {

                Intent intent = new Intent(getBaseContext(), EditarNote.class);
                intent.putExtra("id", String.valueOf(position));
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
            atualizarListView();
    }

    public void addNote(View view) {
        EditText titleText = (EditText) findViewById(R.id.editTextTitle);
        EditText contentText = (EditText) findViewById(R.id.editTextContent);
        ContentValues values = new ContentValues();
        values.put(NotesProvider.TITLE, titleText.getText().toString());
        values.put(NotesProvider.CONTENT, contentText.getText().toString());
        values.put(NotesProvider.DIA, dia);
        values.put(NotesProvider.MES, mes);
        values.put(NotesProvider.ANO, ano);


        Uri uri = getContentResolver().insert(NotesProvider.CONTENT_URI, values);
        Toast.makeText(getBaseContext(), uri.toString(), Toast.LENGTH_LONG).show();
        atualizarListView();
    }

    public void listNotes(View view) {
        List<String> list = new ArrayList<String>();
        String URL = NotesProvider.URL;
        Uri notesURI = Uri.parse(URL);
        Cursor cursor = getContentResolver().query(notesURI, null, null, null, NotesProvider.ID);
        if (cursor.moveToFirst()) {
            do {
                Note note = new Note();
                note.setId(cursor.getInt(cursor.getColumnIndex(NotesProvider.ID)));
                note.setTitle(cursor.getString(cursor.getColumnIndex(NotesProvider.TITLE)));
                note.setContent(cursor.getString(cursor.getColumnIndex(NotesProvider.CONTENT)));
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR,Integer.parseInt(cursor.getString(cursor.getColumnIndex(NotesProvider.ANO))));
                calendar.set(Calendar.MONTH,Integer.parseInt(cursor.getString(cursor.getColumnIndex(NotesProvider.MES))));
                calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(cursor.getString(cursor.getColumnIndex(NotesProvider.DIA))));
                note.setDate(calendar);
                list.add(note.toString());
            } while (cursor.moveToNext());
        }

        final StableArrayAdapter adapter = new StableArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public void showDatePickerDialog(View view) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            startDateDefault();

            return new DatePickerDialog(getActivity(), this, ano, mes, dia);
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            Button data = (Button) findViewById(R.id.date_picker);
            data.setText(day+"/"+month+"/"+year);
            ano = year;
            mes = month;
            dia = day;
        }
    }
    public void startDateDefault() {
        if (ano == 0) {
            Calendar calendar = Calendar.getInstance();
            ano = calendar.get(Calendar.YEAR);
            mes = calendar.get(Calendar.MONTH);
            dia = calendar.get(Calendar.DAY_OF_MONTH);
        }
    }

    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }

    public void atualizarListView(){
        List<String> list = new ArrayList<String>();
        String URL = NotesProvider.URL;
        Uri notesURI = Uri.parse(URL);
        Cursor cursor = getContentResolver().query(notesURI, null, null, null, NotesProvider.ID);
        if (cursor.moveToFirst()) {
            do {
                Note note = new Note();
                note.setId(cursor.getInt(cursor.getColumnIndex(NotesProvider.ID)));
                note.setTitle(cursor.getString(cursor.getColumnIndex(NotesProvider.TITLE)));
                note.setContent(cursor.getString(cursor.getColumnIndex(NotesProvider.CONTENT)));
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR,Integer.parseInt(cursor.getString(cursor.getColumnIndex(NotesProvider.ANO))));
                calendar.set(Calendar.MONTH,Integer.parseInt(cursor.getString(cursor.getColumnIndex(NotesProvider.MES))));
                calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(cursor.getString(cursor.getColumnIndex(NotesProvider.DIA))));
                note.setDate(calendar);
                list.add(note.toString());
            } while (cursor.moveToNext());
        }

        final StableArrayAdapter adapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);
    }
}
