package br.ufc.dc.dspm.mynotes;

/**
 * Created by Vasco on 16/06/2016.
 */
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Created by Vasco on 14/06/2016.
 */
public class EditarNote extends AppCompatActivity {

    Note note;
    static int dia;
    static int mes;
    static int ano;

    EditText edt1;
    EditText edt2;

    Button date;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editar_note);
        getSupportActionBar();


        Integer id = Integer.parseInt(getIntent().getStringExtra("id"));

        //query in notes provider
        this.note = findNote(id);


        edt1 = (EditText) findViewById(R.id.editText);
        edt2 = (EditText) findViewById(R.id.editText2);
        date = (Button) findViewById(R.id.button2);

        edt1.setText(note.getTitle());
        edt2.setText(note.getContent());
        date.setText(note.getDateString());


    }

    public Note findNote(Integer id){
        int count_i = 0;
        Note note = null;
        String URL = NotesProvider.URL;
        Uri notesURI = Uri.parse(URL);
        Cursor cursor = getContentResolver().query(notesURI, null, null, null, NotesProvider.ID);
        if(cursor.moveToFirst()){
            do{
                if(count_i == id){
                    note = new Note();
                    note.setId(cursor.getInt(cursor.getColumnIndex(NotesProvider.ID)));
                    note.setTitle(cursor.getString(cursor.getColumnIndex(NotesProvider.TITLE)));
                    note.setContent(cursor.getString(cursor.getColumnIndex(NotesProvider.CONTENT)));
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR,Integer.parseInt(cursor.getString(cursor.getColumnIndex(NotesProvider.ANO))));
                    calendar.set(Calendar.MONTH,Integer.parseInt(cursor.getString(cursor.getColumnIndex(NotesProvider.MES))));
                    calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(cursor.getString(cursor.getColumnIndex(NotesProvider.DIA))));
                    note.setDate(calendar);
                }
                count_i++;
            }while (cursor.moveToNext() && note == null);
        }

        return note;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editar_note, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.salvar){
            //Salvar no banco, atualizar nó
            Calendar calendar = Calendar.getInstance();
            calendar.set(ano, mes, dia);

            note.setTitle(edt1.getText().toString());
            note.setContent(edt2.getText().toString());
            note.setDate(calendar);

            //noteDAO.update(this.note);
            Toast.makeText(this,"Ainda não foi implementado",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,MyNotes.class);
            startActivity(intent);
            finish();
        }
        else if(item.getItemId() == R.id.excluir){
            //Excluir do banco
            //noteDAO.delete(note.getId());
            Toast.makeText(this,"Ainda não foi implementado",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,MyNotes.class);
            startActivity(intent);
            finish();

        }

        return super.onOptionsItemSelected(item);
    }

    public void showDateEditorDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            dia = day;
            mes = month;
            ano = year;
        }
    }
}

