package br.ufc.dc.dspm.mynotes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Note {

    private int id;

    private String title;

    private String content;

    private Calendar date = Calendar.getInstance();

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String toString() {
        return "(" + id + ") "+ "Data: "+this.date.get(Calendar.DAY_OF_MONTH) + "/"+ this.date.get(Calendar.MONTH)+ "/"+ this.date.get(Calendar.YEAR)+" - " + title + ": " + content ;
    }


    public void setDateCalendar(int dia,int mes,int ano) throws ParseException {
        this.date.set(ano,mes,dia);
    }

    public String getDateString(){
        return this.date.get(Calendar.DAY_OF_MONTH) + "/"+ this.date.get(Calendar.MONTH)+ "/"+ this.date.get(Calendar.YEAR);
    }
}