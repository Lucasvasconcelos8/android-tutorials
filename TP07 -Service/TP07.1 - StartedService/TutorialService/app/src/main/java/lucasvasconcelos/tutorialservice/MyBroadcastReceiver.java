package lucasvasconcelos.tutorialservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Vasco on 10/05/2016.
 */
    public class MyBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, "Broadcast Received!!", Toast.LENGTH_SHORT).show();
            if(intent.getAction().equalsIgnoreCase("android.intent.action.SCREN_ON") || intent.getAction().equalsIgnoreCase("android.intent.action.BOOT_COMPLETED") ){
                context.startService(new Intent(context,StartedService.class));
            }
        }
    }
