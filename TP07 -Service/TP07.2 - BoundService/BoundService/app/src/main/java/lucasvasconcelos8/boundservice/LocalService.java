package lucasvasconcelos8.boundservice;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import java.util.Random;

/**
 * Created by Vasco on 11/06/2016.
 */
public class LocalService extends Service {
    private final IBinder binder = new LocalBinder();
    private final Random generator = new Random();

    public class LocalBinder extends Binder {
        LocalService getService() {
            return LocalService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public int getRandomNumber() {
        return generator.nextInt(100);
    }

    public float soma(float a, float b) {
        return a+b;
    }
    public float subtracao(float a, float b) {
        return a-b;
    }
    public float divisao(float a, float b) {
        return a/b;
    }
    public float multiplicacao(float a, float b) {
        return a*b;
    }
}
