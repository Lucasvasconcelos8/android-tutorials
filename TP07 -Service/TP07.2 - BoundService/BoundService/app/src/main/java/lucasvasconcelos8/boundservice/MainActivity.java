package lucasvasconcelos8.boundservice;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    LocalService local_service;
    boolean isBound = false;

    Button soma;
    Button subtracao;
    Button multiplicacao;
    Button divisao;

    TextView resultado;
    EditText primeiroNumero;
    EditText segundoNumero;

    float result;

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocalService.LocalBinder binder = (LocalService.LocalBinder) service;
            local_service = binder.getService();
            isBound = true;
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        soma = (Button)findViewById(R.id.soma_button);
        subtracao = (Button)findViewById(R.id.sub_button);
        multiplicacao = (Button)findViewById(R.id.multi_button);
        divisao = (Button) findViewById(R.id.div_button);

        resultado = (TextView)findViewById(R.id.resultado);
        primeiroNumero = (EditText)findViewById(R.id.entrada1);
        segundoNumero = (EditText)findViewById(R.id.entrada2);
    }


    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, LocalService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }
    @Override
    protected void onStop() {
        super.onStop();
        if (isBound) {
            unbindService(connection);
            isBound = false;
        }
    }

    public void soma(View view) {
        if (isBound) {
            result = local_service.soma(Float.parseFloat(primeiroNumero.getText().toString()) ,Float.parseFloat(segundoNumero.getText().toString()));
            resultado.setText("Resultado : "+String.valueOf(result));
        }
    }
    public void subtracao(View view) {
        if (isBound) {
            result = local_service.subtracao(Float.parseFloat(primeiroNumero.getText().toString()), Float.parseFloat(segundoNumero.getText().toString()));
            resultado.setText("Resultado : "+String.valueOf(result));
        }
    }
    public void divisao(View view) {
        if (isBound) {
            result = local_service.divisao(Float.parseFloat(primeiroNumero.getText().toString()), Float.parseFloat(segundoNumero.getText().toString()));
            resultado.setText("Resultado : "+String.valueOf(result));
        }
    }
    public void multiplicacao(View view) {
        if (isBound) {
            result = local_service.multiplicacao(Float.parseFloat(primeiroNumero.getText().toString()), Float.parseFloat(segundoNumero.getText().toString()));
            resultado.setText("Resultado : "+String.valueOf(result));
        }
    }
}
