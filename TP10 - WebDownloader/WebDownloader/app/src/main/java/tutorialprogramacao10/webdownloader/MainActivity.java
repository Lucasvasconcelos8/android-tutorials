package tutorialprogramacao10.webdownloader;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    EditText edt_url;
    Button bt_download;
    TextView url_result;
    private ConnectivityManager connManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkConnection(this.getCurrentFocus());

        edt_url = (EditText) findViewById(R.id.insert_url);
        bt_download = (Button) findViewById(R.id.button_download);
        url_result = (TextView) findViewById(R.id.url_result);
        url_result.setMovementMethod(new ScrollingMovementMethod());

        bt_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Cria a rotina de fazer o download da pagina indicada no app
                DownloaderTask downTask = new DownloaderTask(url_result);

                downTask.execute(edt_url.getText().toString());


            }
        });

    }

    public void checkConnection(View view){

        connManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

        NetworkInfo info = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean isWifiConn = info.isConnected();
        Toast.makeText(this, "Is Wi-fi connected?" + isWifiConn, Toast.LENGTH_SHORT).show();

        info =  connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        boolean isMobileConn = info.isConnected();
        Toast.makeText(this,"Is Mobile connected?"+isMobileConn,Toast.LENGTH_SHORT).show();
    }
}
