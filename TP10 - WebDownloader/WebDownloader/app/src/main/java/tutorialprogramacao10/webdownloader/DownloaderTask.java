package tutorialprogramacao10.webdownloader;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Vasco on 24/05/2016.
 */
public class DownloaderTask extends AsyncTask<String,String,String>{

    private ConnectivityManager connManager;
    private TextView textView;

    public DownloaderTask(TextView textView){
        this.textView = textView;
    }

    @Override
    protected String doInBackground(String... params) {


        String content = (String)params[0];
        String result = null;

        try{
            result = connectAndRead(content);
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return content;
    }

    protected void OnProgressUpdate(String result){}

    protected void onPostExecute(String result){
        this.textView.setText(result);
    }

    private String connectAndRead(String url_receive) throws IOException {

        URL url = new URL(url_receive);
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();

        conn.setReadTimeout(10000);
        conn.setConnectTimeout(15000);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        conn.connect();

        InputStream is = conn.getInputStream();
        BufferedReader reader;
        reader = new BufferedReader(new InputStreamReader(is,"UTF-8"));
        String data = null;
        String content = "";
        while((data = reader.readLine()) != null){
            content += data + "\n";
        }

        return content;
    }
}
