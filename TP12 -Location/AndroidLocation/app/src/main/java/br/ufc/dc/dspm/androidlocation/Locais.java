package br.ufc.dc.dspm.androidlocation;

/**
 * Created by Vasco on 10/06/2016.
 */
public class Locais {

    private double latitude;
    private double longitude;

    public Locais(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
