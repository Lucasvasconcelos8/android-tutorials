package br.ufc.dc.dspm.androidlocation;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AndroidLocation extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private TextView addressLabel;
    private TextView locationLabel;
    private GoogleApiClient googleApiClient;

    private  ArrayList<Locais> locais = new ArrayList<Locais>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_location);
        this.addressLabel = (TextView) findViewById(R.id.addressTextView);
        this.locationLabel = (TextView) findViewById(R.id.locationTextView);

        GoogleApiClient.Builder builder = new GoogleApiClient.Builder(this);
        builder.addApi(LocationServices.API);
        builder.addConnectionCallbacks(this);
        builder.addOnConnectionFailedListener(this);
        googleApiClient = builder.build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
        locationLabel.setText("Got connected...");
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        locationLabel.setText("Got disconnected...");
        super.onStop();

    }

    public void doConnect(View view) {
        googleApiClient.connect();
        locationLabel.setText("Got connected...");
    }

    public void doDisconnect(View view) {
        googleApiClient.disconnect();
        locationLabel.setText("Got disconnected...");
    }

    public void getLocation(View view) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        Locais local = new Locais(location.getLatitude(),location.getLongitude());

        if(locais.size() < 4){
            locais.add(local);
            String text = "Location = <" + location.getLatitude() + "," + location.getLongitude() + ">";
            this.locationLabel.setText(text);
            Toast.makeText(this,"Local adicionado",Toast.LENGTH_SHORT).show();

            if(locais.size() == 4) {
                Toast.makeText(this, "Todos os locais foram adicionados", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            Toast.makeText(this, "Avaliando se local está dentro dos limites...",Toast.LENGTH_SHORT).show();
            areaTeste(local);
        }

        GetAddressTask task = new GetAddressTask(this);
        task.execute(location);
    }

    public void limparLocais(View view){
        this.locais.clear();
        Toast.makeText(this, "Todos os locais foram deletados", Toast.LENGTH_SHORT).show();
    }

    public void areaTeste(Locais local){
        double minLatitude;
        double maxLatitude;
        double minLongitude;
        double maxLongitude;

        if(this.locais.size() != 4){
            Toast.makeText(this,"Area ainda não definida",Toast.LENGTH_SHORT).show();
        }
        else{
            minLatitude = locais.get(0).getLatitude();
            maxLatitude = locais.get(0).getLatitude();
            minLongitude = locais.get(0).getLongitude();
            maxLongitude = locais.get(0).getLongitude();

            for(int i =1 ; i< locais.size() ; i++){
                if(locais.get(i).getLatitude() < minLatitude){
                    minLatitude = locais.get(i).getLatitude();
                }
                else if(locais.get(i).getLatitude() > maxLatitude){
                    maxLatitude = locais.get(i).getLatitude();
                }
                else if(locais.get(i).getLongitude() < minLongitude){
                    minLongitude = locais.get(i).getLongitude();
                }
                else if (locais.get(i).getLongitude() > maxLongitude){
                    maxLongitude = locais.get(i).getLongitude();
                }
            }

            if(local.getLatitude() >= minLatitude && local.getLatitude() <= maxLatitude && local.getLongitude() >= minLongitude && local.getLongitude() <= maxLongitude){
                Toast.makeText(this,"Local pertence a área",Toast.LENGTH_SHORT).show();
                String text = "Location = <" + local.getLatitude() + "," + local.getLongitude() + ">";
                this.locationLabel.setText(text);
            }
            else{
                Toast.makeText(this,"Local não pertence a área",Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void doSubscribe(View view) {
        if (googleApiClient.isConnected()) {
            LocationRequest request = new LocationRequest();
            request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            request.setInterval(5000);
            request.setSmallestDisplacement(2);

            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, request, this);
        }
    }


    public void doUnsubscribe(View view) {
        if (googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Toast.makeText(this, "Connected!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int value) {
        Toast.makeText(this, "Disconnected!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Toast.makeText(this, "Connection failed...", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        String text = "Updated Location = <" + location.getLatitude() + "," + location.getLongitude() + ">";
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    private class GetAddressTask extends AsyncTask<Location, Void, String> {
        private Context context;

        public GetAddressTask(Context context) {
            super();
            this.context = context;
        }

        @Override
        protected void onPostExecute(String address) {
            addressLabel.setText(address);
        }

        @Override
        protected String doInBackground(Location... params) {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            Location location = params[0];
            List<Address> addresses;
            try {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            } catch (IOException ioe) {
                Log.e("GetAddressTask", "IO Exception in getFromLocation()");
                ioe.printStackTrace();
                return "IO Exception trying to get address";
            } catch (IllegalArgumentException iae) {
                String errorString = "Illegal arguments " + Double.toString(location.getLatitude()) + " , " + Double.toString(location.getLongitude()) + " passed to address service";
                Log.e("GetAddressTask", errorString);
                iae.printStackTrace();
                return errorString;
            }
            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                String addressText = address.getAddressLine(0) + ", " + address.getAdminArea() + ", " + address.getCountryCode();
                return addressText;
            } else {
                return "No address found!";
            }
        }
    }
}
