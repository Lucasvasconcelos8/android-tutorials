package lucasvasconcelos8.monitorsensors;

import android.annotation.TargetApi;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager mSensorManager;
    TextView tv_temperature;
    TextView tv_pressure;
    TextView tv_humidity;
    TextView tv_light;

    Button bt_ativar;
    Button bt_desativar;

    Sensor temperature;
    Sensor light;
    Sensor pressure;
    Sensor humidity;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_temperature = (TextView)findViewById(R.id.temperature_value);
        tv_pressure = (TextView)findViewById(R.id.pressure_value);
        tv_humidity = (TextView)findViewById(R.id.humidity_value);
        tv_light = (TextView)findViewById(R.id.light_value);

        bt_ativar = (Button)findViewById(R.id.bt_ativar);
        bt_desativar = (Button)findViewById(R.id.bt_desativar);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        if(mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE) == null){
            tv_temperature.setText("No temperature sensor");
        }else{
            temperature = mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        }

        if(mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT) == null){
            tv_light.setText("No light sensor");
        }
        else{
            light = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        }
        if(mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE) == null){
            tv_pressure.setText("No pressure sensor");
        }else {
            pressure = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        }
        if(mSensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY) == null){
            tv_humidity.setText("No humidity sensor");
        }else {
            humidity = mSensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        }

    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if(event.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE){
            //temperature in celsius
            Toast.makeText(this,String.valueOf(event.values[0]),Toast.LENGTH_SHORT).show();
            float celsius = event.values[0];
            tv_temperature.setText(String.valueOf(celsius)+"Cº");
        }

        if(event.sensor.getType() == Sensor.TYPE_LIGHT){
            //SI lux units
            float SI_lux = event.values[0];
            tv_light.setText(String.valueOf(SI_lux)+" SI lux");
        }

        if(event.sensor.getType() == Sensor.TYPE_PRESSURE){
            float millibars_of_pressure = event.values[0];
            tv_pressure.setText(String.valueOf(millibars_of_pressure)+" Millibars");
        }

        if(event.sensor.getType() == Sensor.TYPE_RELATIVE_HUMIDITY){
            //percent umidity
            Toast.makeText(this,String.valueOf(event.values[0]),Toast.LENGTH_SHORT).show();
            float percent_umidity = event.values[0];
            tv_humidity.setText(String.valueOf(percent_umidity)+"%");
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void ativar(View view){
        // Register a listener for the sensor.
        mSensorManager.registerListener(this, temperature, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, light, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, pressure, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, humidity, SensorManager.SENSOR_DELAY_NORMAL);
    }
    public void desativar(View view){
        // Be sure to unregister the sensor when the activity pauses.
        mSensorManager.unregisterListener(this);
    }

}

